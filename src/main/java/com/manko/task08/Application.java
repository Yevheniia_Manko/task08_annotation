package com.manko.task08;

import com.manko.task08.view.ConsoleView;

public class Application {

    public static void main(String[] args) {
        ConsoleView view = new ConsoleView();
        view.show();
    }
}
