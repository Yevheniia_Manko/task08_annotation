package com.manko.task08.view;

import com.manko.task08.model.MyCustomAnnotation;
import com.manko.task08.model.MyCustomClass;
import com.manko.task08.model.MyGenericClass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ConsoleView {

    private MyCustomClass customClass;
    private Class clazz;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner scanner;
    private static Logger logger = LogManager.getLogger(ConsoleView.class);

    public ConsoleView() {
        customClass = new MyCustomClass();
        clazz = customClass.getClass();
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Print fields with MyCustomAnnotation.");
        menu.put("2", "  2 - Print all declared methods.");
        menu.put("3", "  3 - Print the result of invoking for public methods.");
        menu.put("4", "  4 - Print the result of invoking for private methods.");
        menu.put("5", "  5 - Set a field value not knowing its type and then print its value.");
        menu.put("6", "  6 - Print the results of invoking myMethod().");
        menu.put("7", "  7 - Print all information about Class for unknown type object.");
        menu.put("Q", "  Q - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::printFieldsWithAnnotation);
        methodsMenu.put("2", this::printAllMethods);
        methodsMenu.put("3", this::printResultInvokingPublicMethods);
        methodsMenu.put("4", this::printResultInvokingPrivateMethods);
        methodsMenu.put("5", this::setAndPrintFieldValue);
        methodsMenu.put("6", this::printResultForMyMethod);
        methodsMenu.put("7", this::printInfoAboutClass);
        methodsMenu.put("Q", this::exit);
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, input menu item:");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.info("Incorrect menu item!");
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        logger.info("\nMENU");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    private void printFieldsWithAnnotation() {
        Field[] fields = clazz.getDeclaredFields();
        logger.info("Field name " + "\t" + "Field type" + "\t\t" + "Annotation");
        for (Field field : fields) {
            if (field.isAnnotationPresent(MyCustomAnnotation.class)) {
                logger.info(field.getName() + "\t" + field.getType().getSimpleName() + "\t\t" +
                        field.getAnnotation(MyCustomAnnotation.class));
            } else {
                logger.info(field.getName() + "\t" + field.getType().getSimpleName() + "\t\tWithout annotation");
            }
        }
    }

    private void printAllMethods() {
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            logger.info(Modifier.toString(method.getModifiers()) + " " + method.getReturnType().getSimpleName() +
                    " " + method.getName() + " " + Arrays.toString(method.getParameterTypes()));
        }
    }

    private void printResultInvokingPublicMethods() {
        try {
            logger.info("Result of invoking for method1:");
            Method method1 = clazz.getDeclaredMethod("someMethodOne", int.class);
            method1.invoke(customClass, 7);
            logger.info("Result of invoking for method2:");
            Method method2 = clazz.getDeclaredMethod("someMethodTwo", int.class, String.class);
            int res = (int) method2.invoke(customClass, 25, "Bobby");
            logger.info(res);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void printResultInvokingPrivateMethods() {
        logger.info("Result of invoking for private method:");
        try {
            Method method = clazz.getDeclaredMethod("methodWithString", String.class);
            method.setAccessible(true);
            String res = method.invoke(customClass, "Alphabet: ").toString();
            logger.info(res);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void setAndPrintFieldValue() {
        try {
            Field field = clazz.getDeclaredField("someNumber");
            if (Modifier.isPrivate(field.getModifiers())) {
                field.setAccessible(true);
            }
            setFieldValue(field, 13);
            logger.info(field.get(customClass));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            logger.info(e.getMessage());
        }
    }

    private void setFieldValue(Field field, Object value) {
        try {
            if (field.getType() == int.class) {
                logger.info("int type");
                field.setInt(customClass, Integer.parseInt(value.toString()));
            }
            if (field.getType() == byte.class) {
                field.setByte(customClass, Byte.parseByte(value.toString()));
            }
            if (field.getType() == short.class) {
                field.setShort(customClass, Short.parseShort(value.toString()));
            }
            if (field.getType() == long.class) {
                field.setLong(customClass, Long.parseLong(value.toString()));
            }
            if (field.getType() == float.class) {
                field.setFloat(customClass, Float.parseFloat(value.toString()));
            }
            if (field.getType() == double.class) {
                field.setDouble(customClass, Double.parseDouble(value.toString()));
            }
            if (field.getType() == boolean.class) {
                field.setBoolean(customClass, Boolean.parseBoolean(value.toString()));
            }
            if (field.getType() == char.class) {
                field.setChar(customClass, value.toString().charAt(0));
            }
            if (!field.getType().isPrimitive()) {
                field.set(customClass, value);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void printResultForMyMethod() {
        try {
            Method method1 = clazz.getDeclaredMethod("myMethod", String.class, int[].class);
            String arg1 = "The sum of int arguments is ";
            String res1 = (String) method1.invoke(customClass, arg1, new int[]{1, 2, 3, 4});
            logger.info("Result of invoking first method myMethod(): ");
            logger.info(res1);
            Method method2 = clazz.getDeclaredMethod("myMethod", java.lang.String[].class);
            String res2 = (String) method2.invoke(customClass, new Object[]{new String[]{"abc", "123"}});
            logger.info("Result of invoking second method myMethod(): ");
            logger.info(res2);
        }  catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void printInfoAboutClass() {
        logger.info("The info about " + clazz.getSimpleName() + " is:");
        MyGenericClass<MyCustomClass> myGenericClass = new MyGenericClass<>(clazz);

        /*
        Class clazz1 = "123".getClass();
        logger.info("The info about " + clazz1.getSimpleName() + " is:");
        MyGenericClass<MyCustomClass> myGenericClass1 = new MyGenericClass<>(clazz1);
        */
    }

    private void exit() {
        logger.info("You finished the application work.");
    }
}
