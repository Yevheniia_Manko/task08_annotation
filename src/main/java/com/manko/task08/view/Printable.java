package com.manko.task08.view;

@FunctionalInterface
public interface Printable {
  void print();
}
