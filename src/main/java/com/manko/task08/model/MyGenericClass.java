package com.manko.task08.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.*;
import java.util.Arrays;

public class MyGenericClass<T> {

    private static Logger logger = LogManager.getLogger(MyGenericClass.class);
    private T obj;
    private final Class<T> clazz;

    public MyGenericClass(Class<T> clazz) {
        this.clazz = clazz;
        try {
            obj = clazz.newInstance();
            logger.info("The superclass is " + clazz.getSuperclass());
            logger.info("\nThe declared fields are:");
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                logger.info(Modifier.toString(field.getModifiers()) + " " + field.getType().getSimpleName() +
                        " " + field.getName());
            }
            logger.info("\nThe declared methods are:");
            Method[] methods = clazz.getDeclaredMethods();
            for (Method method : methods) {
                logger.info(Modifier.toString(method.getModifiers()) + " " + method.getReturnType().getSimpleName() +
                        " " + method.getName() + " " + Arrays.toString(method.getParameterTypes()));
            }
            Constructor[] constructors = clazz.getDeclaredConstructors();
            logger.info("\nThe declared constructors are:");
            for (Constructor constructor : constructors) {
                logger.info(constructor.toString());
            }
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
