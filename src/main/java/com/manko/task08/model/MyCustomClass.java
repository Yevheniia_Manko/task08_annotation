package com.manko.task08.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyCustomClass {

    @MyCustomAnnotation(name = "Dick")
    private String someString;
    @MyCustomAnnotation(name = "Ann", alias = "Green Gables")
    private double someNumber;
    public String publicStr = "yes";
    public boolean isPublic = true;
    public Integer num;
    private static Logger logger = LogManager.getLogger(MyGenericClass.class);

    public MyCustomClass() {
    }

    private MyCustomClass(String someString) {
        this.someString = "";
    }

    public MyCustomClass(String someString, double someNumber) {
        this.someString = someString;
        this.someNumber = someNumber;
    }

    public String getSomeString() {
        return someString;
    }

    public void setSomeString(String someString) {
        this.someString = someString;
    }

    public double getSomeNumber() {
        return someNumber;
    }

    public void setSomeNumber(double someNumber) {
        this.someNumber = someNumber;
    }

    public void someMethodOne(int number) {
        @MyCustomAnnotation(name = "Rose", someValue = 12)
        int count = 100;
        logger.info(count + number);
    }

    public int someMethodTwo(int number, String name) {
        int square = number * number;
        logger.info(name + "! Square of number " + number + " is: ");
        return square;
    }

    private String methodWithString(String str) {
        return str + "abcdefg";
    }

    public String myMethod(String a, int... args) {
        int sum = 0;
        for (Integer number : args) {
            sum += number;
        }
        return a + sum;
    }

    public String myMethod(String... args) {
        StringBuilder sb = new StringBuilder("");
        for (String s : args) {
            sb.append(s);
        }
        String str = sb.toString();
        if (str.isEmpty()) {
            return "Method was invoke without parameters.";
        } else {
            return str;
        }
    }
}
